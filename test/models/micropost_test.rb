require 'test_helper'

class MicropostTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = User.new(name:"Example user", email:"user@example.com", password:"foobar", password_confirmation:"foobar")
    @micropost = @user.micropost.build(content:"sample content", user_id: @user.id)
  end
  
  test "should be valid" do
    assert @micropost.valid?
  end
  
  test "user ID should be present" do
    @micropost.user_id = nil
    assert_not @micropost.valid?
  end
  
  test "order of micropost should be most recent first" do
    assert_equal Micropost.first microposts(:most_recent)
  end 
  
  test "content should be present" do
    @micropost.content = nil
    assert_not @micropost.valid?
  end
  
  test "content should have a maximum of 140 characters" do
    @micropost.content = "a"*141
    assert_not @micropost.valid?
  end
end
