require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
   def setup
    @user = User.new(name:"Example user", email:"user@example.com", password:"foobar", password_confirmation:"foobar")
   end
   
   test "should be valid" do
     assert @user.valid? #should be valid
   end
   
   test "name should be present" do
     @user.name = " "
     assert_not @user.valid? #should not be valid
   end
   
   test "email should be present" do
     @user.email = " "
     assert_not @user.valid?
   end
   
    test "name should not be too long" do
     @user.name = "a" * 51
     assert_not @user.valid?
   end
   
   test "email should not be too long" do
     @user.email = "a"*244 + "@example.com"
     assert_not @user.valid?
   end
   
   test "email validation should accept valid email addresses" do
     valid_address = %w[user@example.com USER@example.COM  A_US-ER@foo.bar.org first.last@foo.jb alica_bob@foo.cn]
     valid_address.each do |add|
      @user.email = add
      assert @user.valid?, "#{add.inspect} should be valid"
     end
   end
   
   test "email validation should reject invalid email addresses" do
     invalid_addresses = %w[user@example,com USERexample.COM A_US-ER@foo_bar.org first@last@foo.jb alica@bob+foo.cn]
     invalid_addresses.each do |invalid_add|
      @user.email = invalid_add
      assert_not @user.valid?, "#{invalid_add.inspect} should be invalid"
     end
   end
   
   test "email address should be unique" do
    dup_user = @user.dup
    dup_user.email = dup_user.email.upcase
    @user.save
    assert_not dup_user.valid?
   end
   
   test "password is too short" do
    @user.password = @user.password = "a"*5
    assert_not @user.valid?
   end
end



